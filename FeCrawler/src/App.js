import React from 'react';
import './App.css';
import Search from './components/Search/Search'
import { w3cwebsocket as W3CWebSocket } from "websocket";

// const BE_PROTOCOL = 'ws';
// const BE_HOST = process.env.CRAWLER_BE_HOST || 'localhost';;
// const BE_PORT = process.env.CRAWLER_BE_PORT || 5050;
//const url = `${BE_PROTOCOL}://${BE_HOST}:${BE_PORT}/ws`;
const client = new W3CWebSocket('ws://localhost:5050/ws');


function App() {
  return (
    <div className="App">
      <Search socket={client}/>
    </div>
  );
}

export default App;
