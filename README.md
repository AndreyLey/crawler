**Crawler**

The deployment contains three containers: 1)Front End - UI 2)Back_End - APIs for UI 3)MongoDB - database for saving already crawled pages
with links.

## Deployment can be perform in few ways:

1. Using docker-compose.yaml from root directory "crawler" In this case docker-compose pull the images from public repositories in Docker Hub
   that I have prepared. This way is faster.
   Just run from the folder where is the docker-compose.yaml exist: **docker-compose -f .\docker-compose.yaml up**
2. If you want to build images:
	a. crawler->BeCrawler run **docker build -t <be_tag_name> -f Dockerfile .**
	b. crawler->FeCrawler run **docker build -t <fe_tag_name> -f Dockerfile .**
	c. in docker-compose.yaml under root directory "crawler" change the image names for be and fe services to the tags you gave in prev steps(a,b)
	d. run from the folder where is the docker-compose.yaml exist: **docker-compose -f .\docker-compose.yaml up**

## System usage
After all containers created and run, UI page can be open in a browser: http//localhost:3000.
Doesn't matter which one of the ways you'll be prefer for deployment, the UI exposed on: http//localhost:3000.

In the browser will appear labels with fields you must fill:

**Url to start - url that will be the first for scraping**
**Max depth for searching - the maximum depth to crawl down to from the start url**
**Max pages to search - the max number of pages for the entire scrape job**

then just press Search button to start the scraping. After pressing the Search,the progress of scrapping can be monitoring
by rows under Job Logs label in the realtime:

*RootUrl(page from it started scraping)
  Url(scraped page)
  Title(scraped page title)
  Depth(at which depth is scraped page)	
  Links(amount of links on scraped page)	
  Status(is scraping finished for RootUrl or still in provcess)*

## Solution overview
Solution are using in **websockets** for realtime updating the UI and **mongoDB** as database for saving already scraped pages:

*{"_id":{"$oid":"5f8bed6052f45f4810fd9f2a"},"uid":"061b9ccc-364c","title":"Google","url":"http://google.com","depth":0,"links":["..."]}*

When received the request from UI, backend starts scraping of the pages send via websocket the result about each scraped page to the UI.
If page for scraping is already exist in db then backend service won't fetch it from the web one more time.



